echo "enter the db password"
read -s passwd
project=$(gcloud config get-value project)
zone=$(gcloud config get-value compute/zone)
region=${zone::-2}

cd ~
git clone https://gitlab.com/rippmn/java-gce-workshop.git

##this is to start proxy 
nohup cloud_sql_proxy -instances=${project}:${region}:workshop-db=tcp:3306 &
sleep 5
##execute the db setup via proxy
mysql  -h 127.0.0.1 -u demo -p${passwd}  < ~/java-gce-workshop/database/database.sql

##kill sql proxy
kill $(pgrep cloud_sql_proxy)

##build the app with db
cp ~/java-gce-workshop/end/pom.xml ~/java-gce-workshop/workshop-app/.
cp ~/java-gce-workshop/end/application-db.properties ~/java-gce-workshop/workshop-app/src/main/resources/.
cd ~/java-gce-workshop/workshop-app
./mvnw clean package -DskipTests

#gen ssh key
ssh-keygen -b 2048 -t rsa -f ~/.ssh/google_compute_engine -q -N ""

##copy the deployment to template
gcloud compute scp target/ROOT.war app-template:~/ROOT.war

##copy the script to activate spring "db" profile to template
gcloud compute scp ~/java-gce-workshop/tomcat-config/setenv.sh app-template:~/setenv.sh
##get back to scripts directory

cd ~/java-gce-workshop/scripts
##remotely deploy app and restart tomcat (note the setenv.sh script is simply copied, it may be better to append to existing file)
gcloud compute ssh app-template --command "sudo mv setenv.sh /usr/share/tomcat9/bin/.; sudo rm -rf /var/lib/tomcat9/webapps/ROOT; sudo mv ~/ROOT.war /var/lib/tomcat9/webapps/ROOT.war; sudo systemctl restart tomcat9.service"





